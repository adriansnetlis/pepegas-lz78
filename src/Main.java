import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static class FileHandler {
		public static String read(String path) {
			try {
				Scanner sc = new Scanner(new FileReader(path));
				String out = "";
				while (sc.hasNext())
					out += sc.next();
				sc.close();
				return out;
			} catch (FileNotFoundException e) {
				System.out.println("File " + path + " not found.");
				return null;
			}
		}

		public static byte[] readBytes(String path) {
			try {
				return Files.readAllBytes(Paths.get(path));
			} catch (IOException e) {
				System.out.println("Couldn't read file " + path);
			}
			return null;
		}

		public static void write(String path, String data) {
			try {
				BufferedWriter wr = new BufferedWriter(new FileWriter(path));
				wr.write(data);
				wr.close();
			} catch (IOException e) {
				System.out.println("Failed to write to file " + path);
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (true) {
			String input = sc.nextLine();
			if (input.equals("exit")) {
				break;
			} else if (input.equals("about")) {
				about();
			} else if (input.equals("comp")) {
				comp(sc);
			} else if (input.equals("decomp")) {
				decomp(sc);
			} else if (input.equals("size")) {
				size(sc);
			} else if (input.equals("equal")) {
				equal(sc);
			} else {
				System.out.println("invalid command.");
			}
		}
		sc.close();
	}

	static void about() {
		System.out.println("201RDB191 Viesturs Kažis 11.grupa");
		System.out.println("201REC077 Ivans Guks 11.grupa");
		System.out.println("201RDB204 Luīze Krasovska 11.grupa");
		System.out.println("201RDB144 Karīna Vedļa 11.grupa");
		System.out.println("201RDB128 Adrians Netlis 11.grupa");
		System.out.println("201RDB414 Roberts Pakalniņš 11.grupa");
	}

	static void size(Scanner sc) {
		System.out.print("file name: ");
		String path = sc.nextLine();
		File file = new File(path);
		if (file.exists())
			System.out.println("size: " + file.length() + " bytes");
		else
			System.out.println("File " + path + " not found.");
	}

	static void equal(Scanner sc) {
		System.out.print("first file name: ");
		String path = sc.nextLine();
		File file = new File(path);
		if (!file.exists()) {
			System.out.println("File " + path + " not found.");
			return;
		}
		System.out.print("second file name: ");
		String path2 = sc.nextLine();
		File file2 = new File(path2);
		if (file2.exists()) {
			try {
				System.out.println(Files.mismatch(file.toPath(), file2.toPath()) == -1);
			} catch (IOException e) {
				System.out.println("Failed to compare files");
				e.printStackTrace();
			}
		} else
			System.out.println("File " + path2 + " not found.");
	}

	static void comp(Scanner sc) {
		System.out.print("source file name: ");
		String line = sc.nextLine();
		String data = FileHandler.read(line);
		if (data == null)
			return;
		LZ78 compressor = new LZ78();
		String compressed = compressor.compress(data);
		System.out.print("archive name: ");
		line = sc.nextLine();
		FileHandler.write(line, compressed);
	}

	static void decomp(Scanner sc) {
		System.out.print("archive name: ");
		String line = sc.nextLine();
		byte[] data = FileHandler.readBytes(line);
		LZ78 compressor = new LZ78();
		List<Byte> decompressed = compressor.decompress(data);
		char[] chars = new char[decompressed.size()];
		for (int i = 0; i < chars.length; i++) {
			chars[i] = (char) (byte) decompressed.get(i);
		}
		System.out.print("file name: ");
		line = sc.nextLine();
		String out = new String(chars);
		FileHandler.write(line, out);
	}

}
