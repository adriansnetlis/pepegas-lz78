import java.util.ArrayList;
import java.util.List;

public class LZ78Dict {
	List<Entry> entries;

	public LZ78Dict() {
		this.entries = new ArrayList<Entry>();
		this.entries.add(new Entry((byte) 0, (byte) 0));
	}

	public void addEntry(byte value, byte root) {
		this.entries.add(new Entry(value, root));
	}

	public int findEntry(byte value, byte root) {
		Entry entry = new Entry(value, root);
		for (Entry e : this.entries) {
			if (e.value == entry.value && e.root == entry.root) {
				return this.entries.indexOf(e);
			}
		}
		return 0;
	}

	public int size() {
		return this.entries.size();
	}

	public String output() {
		String output;
		byte[] out = new byte[entries.size() * 2 - 2];
		for (int i = 0; i < entries.size() - 1; i++) {
			out[i * 2] = entries.get(i + 1).value;
			out[i * 2 + 1] = entries.get(i + 1).root;
		}
		output = new String(out);
		return output;
	}

	private class Entry {
		byte value;
		byte root;

		Entry(byte value, byte root) {
			this.value = value;
			this.root = root;
		}
	}

	public List<Byte> getByteSequence(int i) {
		List<Byte> bytes = new ArrayList<Byte>();
		Entry entry = this.entries.get(i);
		int next = entry.root;
		if (next != 0) {
			List<Byte> depth = getByteSequence(next);
			for (byte b : depth)
				bytes.add(b);
		}
		bytes.add(entry.value);
		return bytes;
	}
}