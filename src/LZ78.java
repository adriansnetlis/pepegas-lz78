import java.util.ArrayList;
import java.util.List;

public class LZ78 {
	public LZ78() {

	}

	public String compress(String data) {
		String output = "";

		int i = 0;

		outer_loop: while (true) {
			LZ78Dict dict = new LZ78Dict();
			byte[] byteData = data.getBytes();
			byte currentSymbol = 0;
			byte currentPointer = 0;

			inner_loop: for (; i < byteData.length; i++) {
				currentSymbol = byteData[i];

				byte next = (byte) dict.findEntry(currentSymbol, currentPointer);

				if (next == 0) {
					// No entry found, got to add;
					dict.addEntry(currentSymbol, currentPointer);
				} else if (i == byteData.length - 1) {
					// If the data stream ends we got to make sure last data doesn't get clipped
					dict.addEntry((byte) 0, next);
					output += dict.output();
					System.out.println("Finito");
					break outer_loop;
				}
				currentPointer = next;
				if (currentPointer == 255)
					break inner_loop;
			}
			output += dict.output();
		}
		return output;
	}

	public List<Byte> decompress(byte[] data) {
		LZ78Dict dict = new LZ78Dict();
		for (int i = 0; i < data.length / 2; i++) {
			dict.addEntry(data[i * 2], data[i * 2 + 1]);
		}

		List<Byte> bytes = new ArrayList<Byte>();

		for (int i = 0; i < dict.size(); i++) {
			List<Byte> sequence = dict.getByteSequence(i);
			for (byte b : sequence) {
				bytes.add(b);
			}
		}
		bytes.remove(0); // First element is redundant due to how the algorithm works

		return bytes;
	}
}